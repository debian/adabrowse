AdaBrowse 4.0.3
============================================================================

AdaBrowse is a javadoc-like HTML and XML generator for Ada 95.

The executable is version 4.0.3 for GNAT 5.03a, running on
Windows NT/XP/2k.

Copyright (c) 2002-2005 by Thomas Wolf <twolf@acm.org>. AdaBrowse is free
software, distributed under the GPL (the GNU General Public License). For
more information, see the User's Guide in file "./doc/adabrowse_ug.html".

AdaBrowse doesn't need any special installation. Just copy the executable
wherever you'd like to have it, and make sure it's on your PATH.

If you intend to build AdaBrowse from the sources, see the User's Guide
in file "./doc/adabrowse_ug.html". You really should run the makefile,
not try to build it yourself using gnatmake!

BEFORE USING AdaBrowse, READ THE USER'S GUIDE IN FILE ./doc/adabrowse_ug.html!

(Note: the "nasty" program is just for demo purposes. It is not needed.)

--
TW, 2005-04-28
