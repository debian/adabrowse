-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Environment variable management.</DL>
--
-- <!--
-- Revision History
--
--   18-NOV-2003   TW  Initial version
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with AD.Messages;
with AD.Projects;

with GAL.ADT.Hash_Tables;
with GAL.Storage.Standard;
with GAL.Support.Hashing;

with Util.Environment;
with Util.Strings;

pragma Elaborate_All (GAL.ADT.Hash_Tables);

package body AD.Environment is

   package Env_Tables is
      new GAL.ADT.Hash_Tables
            (Key_Type => String,
             Item     => String,
             Memory   => GAL.Storage.Standard,
             Hash     => GAL.Support.Hashing.Hash_Case_Insensitive,
             "="      => Util.Strings.Equal);

   My_Environment : Env_Tables.Hash_Table;

   function Get
     (Name : in String)
     return String
   is
   begin
      return Env_Tables.Retrieve (My_Environment, Name);
   exception
      when Env_Tables.Container_Empty |
           Env_Tables.Not_Found =>
         --  Check the real environment.
         begin
            declare
               Value : constant String := Util.Environment.Get (Name);
            begin
               Set (Name, Value);
               return Value;
            end;
         exception
            when Util.Environment.Not_Defined =>
               return "";
         end;
   end Get;

   procedure Set
     (Name  : in String;
      Value : in String)
   is
   begin
      AD.Messages.Debug
        ("Registering environment binding " & Name & "=" & Value);
      Env_Tables.Replace (My_Environment, Key => Name, Element => Value);
      AD.Projects.Define_Variable (Name, Value);
   end Set;

   function Is_Defined
     (Name : in String)
     return Boolean
   is
   begin
      if Env_Tables.Contains (My_Environment, Name) then
         return True;
      end if;
      begin
         Set (Name, Util.Environment.Get (Name));
         return True;
      exception
         when Util.Environment.Not_Defined =>
            return False;
      end;
   end Is_Defined;

   procedure Add
     (Assignment : in String)
   is
      --  We have a string name=value. Name must not contain embedded white
      --  space. Leading and trailing white space in name or value is trimmed.
      --  The whole assignment must already be unquoted, if it was enclosed
      --  in quotes.
      use Util.Strings;
      I : Natural;
   begin
      I := First_Index (Assignment, '=');
      if I <= Assignment'First then raise Invalid_Variable_Assignment; end if;
      declare
         Name : constant String :=
           Trim (Assignment (Assignment'First .. I - 1));
         Value : constant String :=
           Trim (Assignment (I + 1 .. Assignment'Last));
      begin
         if Name'Length = 0 or else Next_Blank (Name) /= 0 then
            raise Invalid_Variable_Assignment;
         end if;
         Set (Name, Value);
      end;
   end Add;

begin
   Env_Tables.Set_Resize (My_Environment, 0.75);
   declare
      Linear_Growth : GAL.Support.Hashing.Linear_Growth_Policy (20);
   begin
      Env_Tables.Set_Growth_Policy (My_Environment, Linear_Growth);
   end;
end AD.Environment;



