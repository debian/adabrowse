-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Environment variable management.</DL>
--
-- <!--
-- Revision History
--
--   18-NOV-2003   TW  Initial version
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

package AD.Environment is

   pragma Elaborate_Body;

   Invalid_Variable_Assignment : exception;
   --  May be raised by @Add@.

   function Get
     (Name : in String)
     return String;

   procedure Set
     (Name  : in String;
      Value : in String);

   function Is_Defined
     (Name : in String)
     return Boolean;

   procedure Add
     (Assignment : in String);

end AD.Environment;



