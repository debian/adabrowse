-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Handling of "pathes": prefix URLs defined for cross-references.</DL>
--
-- <!--
-- Revision History
--
--   05-FEB-2002   TW  Initial version.
--   04-JUL-2002   TW  Added exceptions to exclusion and no_xrefs.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

package AD.Exclusions is

   pragma Elaborate_Body;

   ----------------------------------------------------------------------------
   --  Support for selectively not generating cross-references for certain
   --  units.

   procedure Add_No_XRef
     (Key : in String);

   procedure Add_No_XRef_Exception
     (Key : in String);

   function No_XRef
     (Unit_Name : in String)
     return Boolean;

   ----------------------------------------------------------------------------
   --  Support for selectively excluding units from HTML generation.

   procedure Add_Exclusion
     (Key : in String);

   procedure Add_Exclusion_Exception
     (Key : in String);

   function Is_Excluded
     (Unit_Name : in String)
     return Boolean;

   function Skip
     (Unit_Name : in String)
     return Boolean;
   --  Is_Excluded (Unit_Name) or else No_Xref (Unit_Name);

   procedure Clear_Exclusions;

   procedure Clear_Exclusion_Exceptions;

end AD.Exclusions;
