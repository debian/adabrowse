------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Index configuration.</DL>
--
-- <!--
-- Revision History
--
--   28-JUN-2003   TW  Initial version.
--   09-JUL-2003   TW  Added the 'Empty' key.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Exceptions;
with Ada.Strings.Unbounded;

with AD.Config;
with AD.Expressions;

with Util.Strings;

package body AD.Indices.Configuration is

   package ASU renames Ada.Strings.Unbounded;

   use Util.Strings;

   procedure Parse
     (Key   : in String;
      Value : in String)
   is

      procedure Parse
        (Idx   : in Index_Ptr;
         Key   : in String;
         Value : in String)
      is
         K : constant String := To_Lower (Key);
      begin
         if K = "file" then
            Set_File_Name (Idx, Value);
         elsif K = "title" then
            Set_Title (Idx, Value);
         elsif K = "rule" then
            begin
               Set_Rule (Idx, AD.Expressions.Parse (Value));
            exception
               when E : AD.Expressions.Parse_Error =>
                  Ada.Exceptions.Raise_Exception
                    (AD.Config.Invalid_Config'Identity,
                     Ada.Exceptions.Exception_Message (E));
            end;
         elsif K = "structured" then
            declare
               Flag : Boolean;
            begin
               Flag := Boolean'Value (To_Upper (Value));
               Set_Structured (Idx, Flag);
            exception
               when others =>
                  Ada.Exceptions.Raise_Exception
                    (AD.Config.Invalid_Config'Identity,
                     "value must be either 'True' or 'False'");
            end;
         elsif K = "empty" then
            Set_Empty (Idx, Value);
         else
            Ada.Exceptions.Raise_Exception
              (AD.Config.Invalid_Config'Identity,
               "unknown selector """ & Key & '"');
         end if;
      end Parse;

      I : constant Natural := First_Index (Key, '.');

   begin
      --  Key is Name.Selector.
      if I <= Key'First or else I = Key'Last or else
         Identifier (Key) /= I - 1
      then
         Ada.Exceptions.Raise_Exception
           (AD.Config.Invalid_Config'Identity,
            "invalid index name """ & Key & "(must be an identifier " &
            "followed by a selector)");
      end if;
      Parse (Get (Key (Key'First .. I - 1)),
             Key (I + 1 .. Key'Last),
             Value);
   end Parse;

   procedure Verify
     renames AD.Indices.Verify;

   type Idx_Desc is
      record
         Name      : ASU.Unbounded_String;
         File_Name : ASU.Unbounded_String;
         Title     : ASU.Unbounded_String;
         Rule      : ASU.Unbounded_String;
      end record;

   Old_Indices : constant array (Index_Type) of Idx_Desc :=
     (Unit_Index =>
        (Name      => ASU.To_Unbounded_String ("units"),
         File_Name => ASU.To_Unbounded_String ("index"),
         Title     => ASU.To_Unbounded_String ("Unit Index"),
         Rule      => ASU.To_Unbounded_String
                        ("unit")),
      Subprogram_Index =>
        (Name      => ASU.To_Unbounded_String ("subprograms"),
         File_Name => ASU.To_Unbounded_String ("procidx"),
         Title     => ASU.To_Unbounded_String ("Subprogram Index"),
         Rule      => ASU.To_Unbounded_String
                        ("subprogram and not protected")),
      Type_Index =>
        (Name      => ASU.To_Unbounded_String ("types"),
         File_Name => ASU.To_Unbounded_String ("typeidx"),
         Title     => ASU.To_Unbounded_String ("Type Index"),
         Rule      => ASU.To_Unbounded_String
                        ("type or subtype")));

   function Enter
     (Which : in Index_Type)
     return AD.Indices.Index_Ptr
   is
      P : constant AD.Indices.Index_Ptr :=
        Get (ASU.To_String (Old_Indices (Which).Name));
   begin
      if ASU.Length (P.File_Name) = 0 then
         Set_File_Name (P, ASU.To_String (Old_Indices (Which).File_Name));
      end if;
      if ASU.Length (P.Title) = 0 then
         Set_Title (P, ASU.To_String (Old_Indices (Which).Title));
      end if;
      if AD.Expressions.Is_Nil (P.Rule) then
         Set_Rule
           (P,
            AD.Expressions.Parse (ASU.To_String (Old_Indices (Which).Rule)));
      end if;
      return P;
   end Enter;

   procedure Enter_Index
     (Which : in Index_Type)
   is
      Unused : constant AD.Indices.Index_Ptr := Enter (Which);

      pragma Warnings (Off, Unused); --  silence -gnatwa

   begin
      null;
   end Enter_Index;

   procedure Set_File_Name
     (Which : in Index_Type;
      Name  : in String)
   is
      P : constant AD.Indices.Index_Ptr := Enter (Which);
   begin
      Set_File_Name (P, Name);
   end Set_File_Name;

   procedure Set_Title
     (Which : in Index_Type;
      Title : in String)
   is
      P : constant AD.Indices.Index_Ptr := Enter (Which);
   begin
      Set_Title (P, Title);
   end Set_Title;

   procedure Set_Structured
     (Which : in Index_Type;
      Flag  : in Boolean)
   is
      P : constant AD.Indices.Index_Ptr := Enter (Which);
   begin
      Set_Structured (P, Flag);
   end Set_Structured;

end AD.Indices.Configuration;
