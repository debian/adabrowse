-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   File output routines, writing to a file or stdout, as needed.</DL>
--
-- <!--
-- Revision History
--
--   19-MAR-2002   TW  First release.
--   18-JUN-2002   TW  Added 'Debug'.
--   22-NOV-2002   TW  Added description of "-private" option.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Text_IO;

with Util.Strings;

with AD.Version;

pragma Elaborate_All (AD.Version);

package body AD.Messages is

   Level : Verbosity := All_Messages;

   procedure Set_Mode
     (To : in Verbosity)
   is
   begin
      Level := To;
   end Set_Mode;

   function Get_Mode
     return Verbosity
   is
   begin
      return Level;
   end Get_Mode;

   procedure Info
     (Msg : in String)
   is
   begin
      if Level < All_Messages then return; end if;
      Ada.Text_IO.Put_Line (Ada.Text_IO.Current_Error,
                            "adabrowse: Info: " & Msg);
   end Info;

   procedure Warn
     (Msg : in String)
   is
   begin
      if Level < Errors_And_Warnings then return; end if;
      Ada.Text_IO.Put_Line (Ada.Text_IO.Current_Error,
                            "adabrowse: Warning: " & Msg);
   end Warn;

   procedure Error
     (Msg : in String)
   is
   begin
      Ada.Text_IO.Put_Line (Ada.Text_IO.Current_Error,
                            "adabrowse: Error: " & Msg);
   end Error;

   procedure Debug
     (Msg : in String)
   is
   begin
      if Level < Including_Debug then return; end if;
      Ada.Text_IO.Put_Line (Ada.Text_IO.Current_Error,
                            "adabrowse: DBG: " & Msg);
   end Debug;

   LF   : constant Character := ASCII.LF;

   Help : constant String :=
     "NAME" & LF &
     "   adabrowse" & LF &
     LF &
     "SYNOPSIS" & LF &
     "   adabrowse (-h | -help | --help | -?)" & LF &
     "   adabrowse {-v | -version | --version | -f file_name | -g |" & LF &
     "              -c file_name | -s URL | -o file_name | -a | -all | --all |"
     & LF &
     "              (-i | -is | -p | -t) [filename] | (-I | -T) directory"
     & LF &
     "              (-G (html [xml] | xml [html]) | -l | " &
                    "-[-]private |" & LF &
     "              -P file_name | -w(0|1|2|e|w|i|a)}"
     & LF &
     LF &
     "DESCRIPTION" & LF &
     LF &
     "   AdaBrowse " & AD.Version.Get_Version & LF &
     LF &
     "   AdaBrowse generates HTML documentation from Ada 95 specifications."
     & LF &
     "   It can also produce XML 1.0 output instead of or in addition to HTML"
     &  LF &
     "   output." & LF &
     LF &
     "OPTIONS" & LF &
     "   The following options are available in AdaBrowse:" & LF &
     LF &
     "   -h" & LF &
     "   -?" & LF &
     "   -help" & LF &
     "   --help         Writes this help text." & LF &
     "   -a" & LF &
     "   -all" & LF &
     "   --all          Generate HTML not only for the unit given"
     & LF &
     "                  in the -f option, but also for all application units"
     & LF &
     "                  on which it depends semantically (""with""es and"
     & LF &
     "                  parent units)." & LF &
     "   -c filename    Defines a config file for the HTML " &
     "generator." & LF &
     "                  Multiple -c options may be given; the files are" & LF &
     "                  processed in the given order and may overwrite" & LF &
     "                  earlier config settings." & LF &
     "   -f filename    Gives the filename (*.ads) of the spec to process."
     & LF &
     "                  The file name may contain a path; if it starts with"
     & LF &
     "                  '@', AdaBrowse reads unit specs from the given file."
     & LF &
     "                  If it is '-', AdaBrowse reads the unit specs from"
     & LF &
     "                  stdin. Only one -f option may be given."
     & LF &
     "   -g             Generate cross-references to the standard"
     & LF &
     "                  library, too." & LF &
     "   -G {format}    Specify the output format(s). Recognized"
     & LF &
     "                  output format names are ""html"" and ""xml"" (as"
     & LF &
     "                  separate arguments). If no -G option is given, the"
     & LF &
     "                  default output format is html."
     & LF &
     "   -i [filename]  Generate a unit index if unit specs are"
     & LF &
     "                  read from a file and the output does *not* go to"
     & LF &
     "                  stdout." & LF &
     "   -is [filename] As -i, but generates a structured unit " &
     "index." & LF &
     "   -l             Use only the line number in HTML cross-" &
     "references." &  LF &
     "   -o filename    Define the output file name. If not set,"
     & LF &
     "                  the output goes to a file with the name of the input"
     & LF &
     "                  and suffix "".html"". If the filename is '-', output"
     & LF &
     "                  is written to stdout. If the filename is a directory,"
     & LF &
     "                  all generated HTML files will be put there. Only one"
     & LF &
     "                  -o option may be given." & LF &
     "   -p [filename]  As -i, but generates an index of all " &
     "subprograms." & LF &
     "   -P filename    Process the GNAT project file 'file_name'. Only one"
     & LF &
     "                  -P option may be given." & LF &
     "   -private" & LF &
     "   --private      Also process the private part of packages and task "
     & LF &
     "                  and protected declarations." & LF &
     "   -q             ""Quiet"" mode: do not issue warning " &
                        "or info messages." & LF &
     "                  Synonym for ""-w0""." & LF &
     "   -s URL         Defines the URL to the style sheet the"
     & LF &
     "                  generated HTML file shall use. This URL should be"
     & LF &
     "                  relative to the final place where you will put the"
     & LF &
     "                  HTML files! Note that a -s option can be overwritten"
     & LF &
     "                  by a later -c option, if the configuration file " &
     "defines" & LF &
     "                  the key ""Style_Sheet""." & LF &
     "   -t [filename]  As -i, but generates a global index of all"
     & LF &
     "                  types." & LF &
     "   -v" & LF &
     "   -version" & LF &
     "   --version      Print version information of AdaBrowse to"
     & LF &
     "                  stderr." & LF &
     "   -wi            Set the warning level. 'i' can be one of:"
     & LF &
     "                    '0', or 'e'         : print only error messages."
     & LF &
     "                    '1', or 'w'         : print warnings and errors."
     & LF &
     "                    '2', or 'a', or 'i' : print all messages." & LF &
     "                    'D'                 : print even debug information."
     & LF &
     "   -x             If set, AdaBrowse never overwrites existing"
     & LF &
     "                  HTML Files. (May be useful in conjunction with the -a"
     & LF &
     "                  option.)" & LF &
     "   -I directory   Define source pathes. Same semantics as"
     & LF &
     "                  for GNAT." & LF &
     "   -T directory   Define pathes for ASIS to search for tree"
     & LF &
     "                  files (*.adt). Multiple -T options may be given."
     & LF & LF &
     "One of -f or -P must be given on the command line; if there is a -P but"
     & LF &
     "no -f, AdaBrowse processes all source files from the given project.";


   procedure Help_Text
   is
      Start : Natural := Help'First;
      I     : Natural;
   begin
      while Start <= Help'Last loop
         I := Util.Strings.Index (Help (Start .. Help'Last), ASCII.LF);
         if I = 0 then I := Help'Last + 1; end if;
         Ada.Text_IO.Put_Line
           (Ada.Text_IO.Current_Error, Help (Start .. I - 1));
         Start := I + 1;
      end loop;
   end Help_Text;

end AD.Messages;
