-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Global storage of file names.</DL>
--
-- <!--
-- Revision History
--
--   09-AUG-2002   TW  Initial version.
--   22-NOV-2002   TW  Added the "Private_Too" operations.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

package AD.Options is

   pragma Elaborate_Body;

   procedure Set_Private_Too
     (Flag : in Boolean);

   function Private_Too
     return Boolean;

   procedure Set_Output_Name
     (Name : in String);

   procedure Set_Output_Directory
     (Name : in String);

   function Output_Name
     return String;
   --  Returns the full name given in the -o option

   function Output_Directory
     return String;
   --  Returns only the directory part of the name given in the -o option

   procedure Set_Overwrite
     (Allowed : in Boolean);

   function Allow_Overwrite
     return Boolean;

   type File_Handling is (Single_File, Multiple_Files);

   procedure Set_Processing_Mode
     (To : in File_Handling);

   function Processing_Mode
     return File_Handling;

   --  if Processing_Mode is
   --
   --  Single_File:
   --    if Output_Name /= "" then
   --      use Output_Name
   --    else
   --       if Output_Directory /= "" then
   --          Create file in Output_Directory
   --       else
   --          if file-to-process had a path then
   --             Create file in that directory
   --          else
   --             Create file in current directory
   --          end if
   --       end if
   --    end if
   --
   --  Multiple_Files:
   --    Printer that produces several files:
   --      Do not check Output_Name
   --    Printer that produces one file only:
   --      Same as for Single_File

end AD.Options;


