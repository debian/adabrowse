--  This file has been generated automatically by the
--  AdaBrowse configuration tool adconf.
--
--  Generated on 2005-02-23 17:09:02
--
--  DO NOT MODIFY THIS FILE!

pragma License (GPL);

with Prj;
separate (AD.Projects.Impl_Yes)
function Get_Parent
  (Project : in Prj.Project_Id)
  return Prj.Project_Id
is
begin
   return Prj.Projects.Table (Project).Extends;
end Get_Parent;
