-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Project manager implementation.</DL>
--
-- <!--
-- Revision History
--
--   07-JUN-2003   TW  Initial version
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Text_IO;

private package AD.Projects.Impl_Yes is

   procedure Handle_Project_File
     (Name : in String);

   procedure Get_Source_File_List
     (File : in out Ada.Text_IO.File_Type);

   function Get_Source_Directories
     return String;

   function Get_Tree_Directory
     return String;

   function Get_Output_Directory
     return String;

   function Get_Project_File_Name
     return String;

   function Project_Version
     return String;

   procedure Reset
     (On_Error : in Boolean);

   procedure Define_Variable
     (Name  : in String;
      Value : in String);

   procedure Initialize;

end AD.Projects.Impl_Yes;
