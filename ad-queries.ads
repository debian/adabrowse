-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Complex ASIS queries.</DL>
--
-- <!--
-- Revision History
--
--   02-FEB-2002   TW  First release.
--   02-MAR-2002   TW  Added 'Inherited_Original_Operation'.
--   04-MAR-2002   TW  Added 'Full_Unit_Name'.
--   03-JUL-2002   TW  Added 'Set_Standard_Units' and 'Crossref_To_Unit'
--   27-AUG-2002   TW  Added 'Real_Declaration' to find the explicit
--                     declarations of implicitly inherited enumeration
--                     literals.
--   22-NOV-2002   TW  New operation 'Container_Name'.
--   04-JUN-2003   TW  New operations 'Has_Private', 'Visible_Items' and.
--                     'Private_Items'.
--   05-JUN-2003   TW  Changed 'Enclosing_Declaration' such that it works
--                     for any element, not just defining names.
--   06-JUN-2003   TW  'Verify_Defining_Name', 'Name_Definition_Image', and
--                     'Name_Expression_Image' are new.
--                        Also changed 'Full_Unit_Name' to construct the unit
--                     name from scratch to get consistent casing.
--                        Moved 'Set_Standard_Units' and 'Crossref_To_Unit'
--                     to AD.Crossrefs.
--   08-JUL-2003   TW  New operation 'Fully_Qualified_Name'; change in function
--                     'Name_Expression_Image' to also handle attribute
--                     references.
--   18-JUL-2003   TW  Moved most operations to the @Asis2@ library.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Asis;

with AD.Messages;

package AD.Queries is

   pragma Elaborate_Body;

   type Operation_Kind is
     (Overridden_Operation,
      New_Operation,
      Inherited_Operation,
      Inherited_Original_Operation);
   --  Semantics:
   --
   --  Overridden_Operation: oerrides some primitive operation of the parent
   --     type.
   --  New_Operation: a new primitive operation that the parent type doesn't
   --     have.
   --  Inherited_Operation: there is no declaration for this primitive
   --     operation here, it is inherited from the parent.
   --  Inherited_Original_Operation: Ditto, and the operation has never been
   --    overridden in the derivation chain between it's original declaration
   --    and the current type.

   type Operation_Description is
      record
         Is_Controlling_Result : Boolean;
         Kind                  : Operation_Kind;
         Decl                  : Asis.Declaration;
      end record;

   type Operation_List is array (Positive range <>) of Operation_Description;

   function Primitive_Operations
     (The_Type : in Asis.Declaration)
     return Operation_List;
   --  'The_Type' should be a public declaration. Returns all publicly
   --  visible operations of the type. The returned array starts at index 1.

   function Ancestor_Type
     (The_Type : in Asis.Declaration)
     return Asis.Declaration;
   --  Returns a Nil_Element if the type doesn't have an ancestor.

   function Is_Ancestor
     (Ancestor : in Asis.Declaration;
      Child    : in Asis.Declaration)
     return Boolean;
   --  returns True if 'Child' is derived directly or indirectly from
   --  'Ancestor'.

   function Is_Primitive
     (Decl     : in Asis.Declaration;
      The_Type : in Asis.Declaration)
     return Boolean;

   function Get_Dependents
     (Unit : in Asis.Compilation_Unit)
     return Asis.Compilation_Unit_List;
   --  Returns the transitive colsure of all withs. The result contains only
   --  application units.

   function Get_Pragmas
     (Unit : in Asis.Compilation_Unit)
     return Asis.Pragma_Element_List;

   function Expand_Generic
     (Element  : in     Asis.Element;
      Reporter : access AD.Messages.Error_Reporter'Class)
     return Asis.Element;
   --  If the <CODE>Element</CODE> is in an implicit generic specification
   --  due to a generic instantiation, returns the corresponding element from
   --  the generic template. Otherwise, returns <CODE>Element</CODE>.

end AD.Queries;
