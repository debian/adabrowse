-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This unit is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Provides dynamic hash tables. Internal collision resolution, automatic
--   and explicit resizing. Collision chain index computation can be customized
--   though <CODE>Collision_Policies</CODE>. Resizing can be controlled through
--   load factors and <CODE>Growth_Policies</CODE>.
--   <BR><BR>
--   This hash table does not allow associating additional data with the
--   items stored. However, only a portion of type @Item@ might be the actual
--   key, while additional components might hold associated data. In this
--   case, both @Hash@ and <CODE>"="</CODE> must work only on the key part
--   of @Item@.
--   <BR><BR>
--   Note that this hash table does not allow in-place modification of the
--   items stored since this might result in violations of the internal
--   consistency of the hash table.
--   <BR><BR>
--   A slightly more powerful (but also slightly more complex to instantiate)
--   hash table package taking separate @Key@ and @Item@ types and allowing
--   in-place modifications of the items (but not the keys) is available in
--   package <A HREF="gal-adt-hash_tables.html">GAL.ADT.Hash_Tables</A>.
--   </DL>
--
-- <DL><DT><STRONG>
-- Tasking semantics:</STRONG><DD>
--   N/A. Not abortion-safe.</DL>
--
-- <DL><DT><STRONG>
-- Storage semantics:</STRONG><DD>
--   Dynamic storage allocation in a user-supplied storage pool.</DL>
--
-- <!--
-- Revision History
--
--   20-DEC-2001   TW  Initial version.
--   28-DEC-2001   TW  Added growth policies.
--   24-APR-2002   TW  Added the 'Choose_Size' generic formal function.
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

--  generic
--     type Item (<>) is private;
--
--     with package Memory is new GAL.Storage.Memory (<>);
--
--     Initial_Size : in GAL.Support.Hashing.Size_Type := 23;
--
--     with function Hash (Element : in Item)     return Hash_Type is <>;
--     with function "="  (Left, Right : in Item) return Boolean   is <>;
--
--     with function Choose_Size
--            (Suggested : in GAL.Support.Hashing.Hash_Type)
--            return GAL.Support.Hashing.Hash_Type
--            is GAL.Support.Hashing.Next_Prime;
--     --  This function is called whenever the size of the hash table is to be
--     --  defined. 'Suggested' is the suggested size of the new table; the
--     --  function should then return a size that is >= Suggested. If it
--     --  returns a smaller value anyway, the exception 'Container_Error' is
--     --  raised.
package body GAL.Containers.Hash_Tables is

   use GAL.Support;

   ----------------------------------------------------------------------------

   procedure Insert
     (Table   : in out Hash_Table;
      Element : in     Item)
   is
   begin
      Impl.Insert (Table.Rep, Element, Null_Object);
   end Insert;

   procedure Insert
     (Table   : in out Hash_Table;
      Element : access Item)
   is
   begin
      Impl.Insert (Table.Rep, Element.all, Null_Object);
   end Insert;

   ----------------------------------------------------------------------------

   procedure Replace
     (Table   : in out Hash_Table;
      Element : in     Item)
   is
   begin
      Impl.Replace (Table.Rep, Element, Null_Object);
   end Replace;

   procedure Replace
     (Table   : in out Hash_Table;
      Element : access Item)
   is
   begin
      Impl.Replace (Table.Rep, Element.all, Null_Object);
   end Replace;

   ----------------------------------------------------------------------------

   procedure Delete
     (Table   : in out Hash_Table;
      Element : in     Item)
   is
   begin
      Impl.Delete (Table.Rep, Element);
   end Delete;

   procedure Delete
     (Table   : in out Hash_Table;
      Element : access Item)
   is
   begin
      Impl.Delete (Table.Rep, Element.all);
   end Delete;

   ----------------------------------------------------------------------------

   function Contains
     (Table   : in Hash_Table;
      Element : in Item)
     return Boolean
   is
   begin
      return Impl.Contains (Table.Rep, Element);
   end Contains;

   function Contains
     (Table   : in     Hash_Table;
      Element : access Item)
     return Boolean
   is
   begin
      return Impl.Contains (Table.Rep, Element.all);
   end Contains;

   ----------------------------------------------------------------------------

   function Nof_Elements
     (Table : in Hash_Table)
     return GAL.Support.Hashing.Hash_Type
   is
   begin
      return Impl.Nof_Elements (Table.Rep);
   end Nof_Elements;

   function Is_Empty
     (Table : in Hash_Table)
     return Boolean
   is
   begin
      return Impl.Is_Empty (Table.Rep);
   end Is_Empty;

   function Load
     (Table : in Hash_Table)
     return GAL.Support.Hashing.Load_Factor
   is
   begin
      return Impl.Load (Table.Rep);
   end Load;

   function Size
     (Table : in Hash_Table)
     return GAL.Support.Hashing.Hash_Type
   is
   begin
      return Impl.Size (Table.Rep);
   end Size;

   ----------------------------------------------------------------------------

   procedure Swap
     (Left, Right : in out Hash_Table)
   is
   begin
      Impl.Swap (Left.Rep, Right.Rep);
   end Swap;

   ----------------------------------------------------------------------------

   procedure Resize
     (Table    : in out Hash_Table;
      New_Size : in     GAL.Support.Hashing.Size_Type)
   is
   begin
      Impl.Resize (Table.Rep, New_Size);
   end Resize;

   ----------------------------------------------------------------------------

   procedure Reset
     (Table : in out Hash_Table)
   is
   begin
      Impl.Reset (Table.Rep);
   end Reset;

   procedure Reset
     (Table    : in out Hash_Table;
      New_Size : in     GAL.Support.Hashing.Size_Type)
   is
   begin
      Impl.Reset (Table.Rep, New_Size);
   end Reset;

   procedure Reset
     (Table     : in out Hash_Table;
      New_Size  : in     GAL.Support.Hashing.Size_Type;
      Resize_At : in     GAL.Support.Hashing.Load_Factor)
   is
   begin
      Impl.Reset (Table.Rep, New_Size, Resize_At);
   end Reset;

   ----------------------------------------------------------------------------

   procedure Merge
     (Result : in out Hash_Table;
      Source : in     Hash_Table)
   is
   begin
      Impl.Merge (Result.Rep, Source.Rep);
   end Merge;

   procedure Merge
     (Result    : in out Hash_Table;
      Source    : in     Hash_Table;
      Overwrite : in     Boolean)
   is
   begin
      Impl.Merge (Result.Rep, Source.Rep, Overwrite);
   end Merge;

   ----------------------------------------------------------------------------
   --  Collision policies:

   procedure Set_Collision_Policy
     (Table  : in out Hash_Table;
      Policy : in     GAL.Support.Hashing.Collision_Policy'Class)
   is
   begin
      Impl.Set_Collision_Policy (Table.Rep, Policy);
   end Set_Collision_Policy;

   procedure Remove_Collision_Policy
     (Table : in out Hash_Table)
   is
   begin
      Impl.Remove_Collision_Policy (Table.Rep);
   end Remove_Collision_Policy;

   function Get_Collision_Policy
     (Table : in Hash_Table)
     return GAL.Support.Hashing.Collision_Policy'Class
   is
   begin
      return Impl.Get_Collision_Policy (Table.Rep);
   end Get_Collision_Policy;

   ----------------------------------------------------------------------------
   --  Growth management:

   procedure Set_Resize
     (Table     : in out Hash_Table;
      Resize_At : in     GAL.Support.Hashing.Load_Factor)
   is
   begin
      Impl.Set_Resize (Table.Rep, Resize_At);
   end Set_Resize;

   procedure Set_Growth_Policy
     (Table  : in out Hash_Table;
      Policy : in     GAL.Support.Hashing.Growth_Policy'Class)
   is
   begin
      Impl.Set_Growth_Policy (Table.Rep, Policy);
   end Set_Growth_Policy;

   procedure Remove_Growth_Policy
     (Table : in out Hash_Table)
   is
   begin
      Impl.Remove_Growth_Policy (Table.Rep);
   end Remove_Growth_Policy;

   function  Has_Growth_Policy
     (Table : in Hash_Table)
     return Boolean
   is
   begin
      return Impl.Has_Growth_Policy (Table.Rep);
   end Has_Growth_Policy;

   function Get_Growth_Policy
     (Table : in Hash_Table)
     return GAL.Support.Hashing.Growth_Policy'Class
   is
   begin
      return Impl.Get_Growth_Policy (Table.Rep);
   end Get_Growth_Policy;

   ----------------------------------------------------------------------------
   --  Traversals:

   procedure Action
     (V     : in out Visitor;
      Key   : in     Item;
      Value : in out GAL.Support.Null_Type;
      Quit  : in out Boolean)
   is
      pragma Warnings (Off, Value); --  silence -gnatwa
   begin
      Execute (Visitor'Class (V), Key, Quit);
   end Action;

   procedure Traverse
     (Table     : in     Hash_Table;
      V         : in out Visitor'Class)
   is
   begin
      Impl.Traverse (Table.Rep, V, False);
   end Traverse;

   --  generic
   --     with procedure Execute
   --          (Value : in     Item;
   --           Quit  : in out Boolean);
   procedure Traverse_G
     (Table : in Hash_Table)
   is
      procedure Exec
        (Key   : in     Item;
         Value : in out GAL.Support.Null_Type;
         Quit  : in out Boolean)
      is
         pragma Warnings (Off, Value); --  silence -gnatwa
      begin
         Execute (Key, Quit);
      end Exec;

      procedure Trav is new Impl.Traverse_G (Exec);

   begin
      Trav (Table.Rep);
   end Traverse_G;

   ----------------------------------------------------------------------------
   --  Comparisons:

   function "="
     (Left, Right : in Hash_Table)
     return Boolean
   is
   begin
      return Impl."=" (Left.Rep, Right.Rep);
   end "=";

end GAL.Containers.Hash_Tables;







