-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This unit is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   A default storage pool that uses the pool of an arbitrary
--   access-to-integer type.</DL>
--
-- <DL><DT><STRONG>
-- Tasking semantics:</STRONG><DD>
--   As all pools, fully task safe.</DL>
--
-- <DL><DT><STRONG>
-- Storage semantics:</STRONG><DD>
--   It's a <EM>storage pool</EM>, so of course it does dynamic storage
--   allocations and deallocations.</DL>
--
-- <!--
-- Revision History
--
--   27-NOV-2001   TW  Initial version.
--   28-APR-2003   TW  Removed the Pool variable that was formerly used to
--                     instantiate GAL.Storage.Standard.
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

with System.Storage_Pools;
with System.Storage_Elements;

package GAL.Storage.Default is

   pragma Elaborate_Body;

   type Manager is
     new System.Storage_Pools.Root_Storage_Pool with null record;
   --  A plain vanilla pool that is the same as the pool for an access-to-
   --  integer type. Note that it is entirely implementation-defined whether
   --  one may use this pool for other types as well.
   --
   --  (Ada 95 *is* lacking a standardized feature to somehow define a
   --  "default" storage pool which, when given in a representation clause,
   --  would effectively make the system use whatever pool it'd use if there
   --  was no representation clause. It turns out that GNAT at least seems to
   --  use one pool for all types, so using 'Standard_Pool' below for the
   --  instantiations of the container packages works all right.)

   procedure Allocate
     (Pool            : in out Manager;
      Storage_Address :    out System.Address;
      Size            : in     System.Storage_Elements.Storage_Count;
      Alignment       : in     System.Storage_Elements.Storage_Count);

   procedure Deallocate
     (Pool            : in out Manager;
      Storage_Address : in     System.Address;
      Size            : in     System.Storage_Elements.Storage_Count;
      Alignment       : in     System.Storage_Elements.Storage_Count);

   function Storage_Size
     (Pool : in Manager)
     return System.Storage_Elements.Storage_Count;

end GAL.Storage.Default;
