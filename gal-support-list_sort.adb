-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Generic list sorting operation with <CODE>O (n*log n)</CODE> worst-case
--   run-time complexity.
--
-- <DL><DT><STRONG>
-- Tasking semantics:</STRONG><DD>
--   N/A. Not abortion-safe.</DL>
--
-- <DL><DT><STRONG>
-- Storage semantics:</STRONG><DD>
--   No dynamic storage allocation. Uses <CODE>O (log n)</CODE> stack
--   space.</DL>
--
-- <!--
-- Revision History
--
--   27-NOV-2001   TW  Initial version.
--   15-JAN-2002   TW  Replaced natural merge sort by a direct method. (Both
--                     are O(n*log n), but the natural merge sort has a higher
--                     constant factor. In my performance tests, natural merge
--                     sort was about three times slower. And, BTW, sorting an
--                     array with GAL.Sorting.Sort_G was about 1.5 to 2 times
--                     faster than this list sort.)
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

--  To consider: shall I rewrite this in terms of a 'Position' type, like the
--  lexicographical comparisons?

--  generic
--     type Node (<>) is limited private;
--     type Node_Ptr is access all Node;
--     with function  "<" (L, R : in Node_Ptr) return Boolean;
--
--     with function  Next     (N       : in Node_Ptr) return Node_Ptr;
--     with procedure Set_Next (N, Next : in Node_Ptr);
--
--     with procedure Post_Process (List, Last : in out Node_Ptr);
procedure GAL.Support.List_Sort
  (List : in out Node_Ptr;
   Last :    out Node_Ptr)
is

   type Sequence is
      record
        First, Last : Node_Ptr;
      end record;

   type Sequences is array (Boolean) of Sequence;

   procedure Split
     (Original : in out Sequence;
      Work     : in out Sequences);
   pragma Inline (Split);

   procedure Split
     (Original : in out Sequence;
      Work     : in out Sequences)
   is
      P    : Node_Ptr := Original.First;
      Q, R : Node_Ptr;
      On   : Boolean := False;
   begin
      while P /= null loop
         R := P;
         loop
            Q := Next (R);
            exit when Q = null or else Q < R;
            R := Q;
         end loop;
         --  Append P .. R to Work (On).
         if Work (On).First = null then
            Work (On).First := P;
         else
            Set_Next (Work (On).Last, P);
         end if;
         Work (On).Last := R;
         P  := Q;
         On := not On;
      end loop;
      Original.First := null; Original.Last := null;
      if Work (False).First /= null then
         Set_Next (Work (False).Last, null);
      end if;
      if Work (True).First /= null then
         Set_Next (Work (True).Last, null);
      end if;
   end Split;

   procedure Merge
     (Work    : in out Sequences;
      Result  : in out Sequence);
   pragma Inline (Merge);

   procedure Merge
     (Work   : in out Sequences;
      Result : in out Sequence)
   is
      P, Q : Node_Ptr;
   begin
      while Work (False).First /= null and then Work (True).First /= null loop
         if Work (True).First < Work (False).First then
            P := Work (True).First;
            Work (True).First := Next (P);
         else
            P := Work (False).First;
            Work (False).First := Next (P);
         end if;
         if Result.First = null then
            Result.First := P;
         else
            Set_Next (Result.Last, P);
         end if;
         Result.Last := P;
      end loop;
      P := null;
      if Work (False).First /= null then
         P := Work (False).First;
         Q := Work (False).Last;
         Work (False).First := null;
      elsif Work (True).First /= null then
         P := Work (True).First;
         Q := Work (True).Last;
         Work (True).First := null;
      end if;
      if P /= null then
         if Result.First = null then
            Result.First := P;
         else
            Set_Next (Result.Last, P);
         end if;
         Result.Last := Q;
      end if;
      Set_Next (Result.Last, null);
   end Merge;

   procedure Sort
     (List : in out Node_Ptr;
      Last :    out Node_Ptr);
   pragma Inline (Sort);

   procedure Sort
     (List : in out Node_Ptr;
      Last :    out Node_Ptr)
   is
      Result : Sequence := (List, null);
      Work   : Sequences;
   begin
      Last := List;
      if List = null or else Next (List) = null then
         --  Zero or one element in the list: no need to sort.
         return;
      end if;
      --  We have at least two elements.
      Split (Result, Work);
      if Work (True).First /= null then
         Sort (Work (False).First, Work (False).Last);
         Sort (Work (True).First, Work (True).Last);
         Merge (Work, Result);
      else
         Result := Work (False);
      end if;
      List := Result.First;
      Last := Result.Last;
   end Sort;

begin
   Sort (List, Last);
   Post_Process (List, Last);
end GAL.Support.List_Sort;


