package Test.Enum is

  type Enumeration is 
    (A, B, C, D);

  type XYZ is new Integer;
  
  procedure Do_Bar (X : in out XYZ);
  
end Test.Enum;
