--  generic
package body Test.Gen.Err is

  procedure Do_X
    (X : in Mix_In'Class)
  is
  begin
     null;
  end Do_X;
    
end Test.Gen.Err;
