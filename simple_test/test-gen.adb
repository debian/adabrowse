--  generic
--     type Base is new Root with private;
package body Test.Gen is

   procedure Do_C
     (M : in Mix_In)
   is
   begin
      null;
   end Do_C;

   procedure Do_C
     (M : in Mix_In;
      X : in Natural)
   is
   begin
      null;
   end Do_C;

   procedure Do_Acc
     (Y : in Mix_In)
   is
   begin
      null;
   end Do_Acc;
     
end Test.Gen;


