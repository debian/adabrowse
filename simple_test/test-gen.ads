generic
   type Base is new Root with private;
package Test.Gen is

   pragma Elaborate_Body;

   type Mix_In is new Base with null record;

   procedure Do_C
     (M : in Mix_In);

   procedure Do_C
     (M : in Mix_In;
      X : in Natural);

   procedure Do_Acc
     (Y : in Mix_In);
   -- We have a comment here!
   
   My_Exception : exception renames an_exception;

   Some_Exception : exception;

   pragma Inline (Do_Acc);
   --  Let's inline this...
   
end Test.Gen;


