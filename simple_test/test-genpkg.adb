package body Test.GenPkg is

   --  generic
   --   type The_Type is private;
   package body Generic_Package is

      Q : Handler;

      procedure Set_Handler
        (X : The_Type;
         P : Handler)
      is
         pragma Warnings (Off, X);
      begin
         Q := P;
      end Set_Handler;

   end Generic_Package;

end Test.GenPkg;
