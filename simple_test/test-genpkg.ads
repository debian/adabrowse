package Test.GenPkg is

   generic
      type The_Type is private;
   package Generic_Package is

      type Handler is access
        function (X : The_Type) return Natural;

      procedure Set_Handler
        (X : The_Type;
         P : Handler);

      generic
         type Second_Type is private;
      package Inner is
         type Handler is access
           function (X : The_Type) return Second_Type;
      end Inner;

   end Generic_Package;

end Test.GenPkg;
