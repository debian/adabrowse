generic
  type X is private;
package Test.Signature is

  procedure X_Proc (A, B : in out X);
  
end Test.Signature;
