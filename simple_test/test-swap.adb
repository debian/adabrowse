procedure Test.Swap (A, B : in out Item)
is
   Tmp : Item := A;
begin
   A := B; B := Tmp;
end Test.Swap;
