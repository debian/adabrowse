with Test.GenPkg;
package Test.Use_GenPkg is

   package My_Gen is
      new Test.GenPkg.Generic_Package (Integer);

   package Inner_Gen is
      new My_Gen.Inner (Integer);

   procedure Dummy
     (V : in Inner_Gen.Handler;
      X : Integer);

end Test.Use_GenPkg;
