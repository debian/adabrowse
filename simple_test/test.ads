-----------------------------------------------------------------------
--
-- <COPYRIGHT> 2002 </COPYRIGHT>
--
-- <INCLUDE_GMGPL>
--
-- <PURPOSE>
--    Just a test package.
-- </PURPOSE>
--
-- <GARBAGE>This is not a user-defined tag.</GARBAGE>
--
-- Generated on <DATE>.
-- <VAR1>
--
-- <!--
--   Revision History:
--      30-APR-2002  TW  Added these comments using user-defined tags.
-- -->
-----------------------------------------------------------------------

package Test is

   type Root is tagged null record;

   procedure Do_A
     (R : in out Root);

   type Parent is new Root with null record;

   procedure Do_B
     (P : in out Parent);
     
   procedure Do_Acc
     (P : access Parent);
     
   procedure Do_Acc
     (P : in Parent);

   An_Exception : exception;
   --  This is just a test exception.
   
   procedure Do_X
     (X : in Root'Class);
     
end Test;
