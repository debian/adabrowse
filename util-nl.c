/******************************************************************************

  Copyright (c) 2001, 2002 by Thomas Wolf.

  This piece of software is free software; you can redistribute it and/or
  modify it under the terms of the  GNU General Public License as published
  by the Free Software  Foundation; either version 2, or (at your option)
  any later version. This unit is distributed in the hope that it will be
  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
  Public License for  more details. You should have received a copy of the
  GNU General Public License with this distribution, see file GPL.txt. If not,
  write to the Free Software Foundation, 59 Temple Place - Suite 330, Boston,
  MA 02111-1307, USA.

  As a special exception from the GPL, if you link this unit with other files
  to produce an executable, this unit does not by itself cause the resulting
  executable to be covered by the GPL. This exception does not however
  invalidate any other reasons why the executable file might be covered by
  the GPL.

  Author:
    Thomas Wolf  (TW)   twolf@acm.org

  Purpose:
    Small C routine to portably get the value of '\n'. (A '\n' in a string
    is replaced at *compile-time*, so there's no portable way to obtain its
    value from Ada 95. And note also that the ISO C standard leaves the actual
    character value of '\n' as well as its external representation in a file
    implementation-defined. Thus we cannot rely on '\n' being the linefeed
    character (Character'Val (10)), nor can we assume that it would appear
    as such in a file (it might just as well end up as a CR-LF or a single
    CR in the file). Hence this minor work-around.)

  Storage semantics:
    No storage allocation.

  Revision History

    29-APR-2002  TW  Initial version.
 *****************************************************************************/

int get_newline (void)
{
  return '\n';
}
